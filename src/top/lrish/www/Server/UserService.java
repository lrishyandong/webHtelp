package top.lrish.www.Server;

import top.lrish.www.entry.User;

import java.util.List;

public interface UserService {
    public boolean selectUserByUser(User user);
    public List<User>findAll();

}
