package top.lrish.www.Server.Imp;

import top.lrish.www.Dao.Imp.UserDaoImp;
import top.lrish.www.Dao.UserDao;
import top.lrish.www.Server.UserService;
import top.lrish.www.entry.User;

import java.util.List;

public class UserServiceImp implements UserService {
    UserDao dao = new UserDaoImp();
    @Override
    public boolean selectUserByUser(User user) {
        User u = dao.findAll(user);
        if (u !=null){
            return true;

        }else {
            return false;
        }
    }

    @Override
    public List<User> findAll() {

        return dao.findAll();
    }
}
