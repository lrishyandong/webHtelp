package top.lrish.www.util;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Properties;

public class JDBCUtil {
    private static DataSource dataSource;
    static {
        try {
            Properties pro = new Properties();
            //通过类加载配置文件
            InputStream is = JDBCUtil.class.getClassLoader().getResourceAsStream("db.properties");
            pro.load(is);
            //为连接池对象赋值
            dataSource = DruidDataSourceFactory
                    .createDataSource(pro);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //获取连接池对象
    public static DataSource getDataSource(){
        return dataSource;
    }
    static ThreadLocal<Connection> t1 = new ThreadLocal<>();
    public static Connection getConn(){
        try {
            Connection conn = t1.get();
            if (conn == null){
                conn = dataSource.getConnection();
                t1.set(conn);
            }
            return conn;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    public static void closeConn(Connection conn, Statement statement){
        try {
            conn.close();
            statement.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println(JDBCUtil.getConn());
    }
}
