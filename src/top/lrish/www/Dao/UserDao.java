package top.lrish.www.Dao;



import top.lrish.www.entry.User;

import javax.jws.soap.SOAPBinding;
import java.util.List;

public interface UserDao {
    public User findAll(User user);
    public List<User> findAll();
    public int updataUser(User user);
}
