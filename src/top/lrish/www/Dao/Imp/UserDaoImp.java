package top.lrish.www.Dao.Imp;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import top.lrish.www.Dao.UserDao;
import top.lrish.www.entry.User;
import top.lrish.www.util.JDBCUtil;


import java.util.List;

public class UserDaoImp implements UserDao {


    @Override
    public User findAll(User user) {
        try{
                QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
                String sql = "select * from user where username=? and password =?";
                Object[] args = {user.getUsername(),user.getPassword()};
                return qr.query(sql,new BeanHandler<>(User.class),args);

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<User> findAll() {
        try{
            QueryRunner qr = new QueryRunner();
            String sql  = "select * from user";
            return qr.query(JDBCUtil.getConn(),sql,new BeanListHandler<>(User.class));
        }catch (Exception e){
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public int updataUser(User user) {
        try {
            QueryRunner qr = new QueryRunner();
            String sql ="UPdate 'user' set username=? ,password=? where uid =?";
            Object[] args ={user.getUsername(),user.getPassword(),user.getUid()};
            return qr.update(JDBCUtil.getConn(),sql,args);
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }


}
