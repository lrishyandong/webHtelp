package top.lrish.www.servlet;

import top.lrish.www.util.VerifyCode;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

@WebServlet("/verify")
public class VerifyServlet  extends HttpServlet {



    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        VerifyCode vc = new VerifyCode();

        BufferedImage image = vc.getImage();
        String text  = vc.getText();
        req.getSession().setAttribute("text",text);
        ImageIO.write(image,"JPEG",resp.getOutputStream());

    }
}
