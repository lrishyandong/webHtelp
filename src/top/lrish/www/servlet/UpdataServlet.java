package top.lrish.www.servlet;

import top.lrish.www.Dao.Imp.UserDaoImp;
import top.lrish.www.Dao.UserDao;
import top.lrish.www.entry.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.text.html.HTML;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/updata")
public class UpdataServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String username=req.getParameter("username");
        String password = req.getParameter("password");
        
        HttpSession session = req.getSession();
        int uid = (Integer)(session.getAttribute("uid"));

        PrintWriter pw  = resp.getWriter();
        UserDao dao = new UserDaoImp();
        dao.updataUser(new User(uid,username,password));


    }
}
