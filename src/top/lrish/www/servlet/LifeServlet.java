package top.lrish.www.servlet;

import javax.jws.WebService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(value = "/life" ,loadOnStartup = 1,
    initParams = {@WebInitParam(name= "username ",value = "tom")})
public class LifeServlet extends HttpServlet {
    public LifeServlet(){
        System.out.println("实例化-->创建当前servlet对象的方法");
    }
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("服务方法--->根据请求的路径执行里面的geT/Dopost");
        resp.setContentType("text/html;charset=utf-8");
        String username = req.getParameter("username");

        PrintWriter pw = resp.getWriter();
        pw.write("hello word");
        pw.write(username);

    }

    @Override
    public void init() throws ServletException {
        System.out.println("初始化方法--->一次");
    }

    @Override
    public void destroy() {
        System.out.println("销毁方法");
    }
}
