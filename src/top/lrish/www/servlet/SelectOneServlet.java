package top.lrish.www.servlet;

import top.lrish.www.Dao.Imp.UserDaoImp;
import top.lrish.www.Dao.UserDao;
import top.lrish.www.entry.User;

import javax.servlet.ServletException;
import javax.servlet.SessionCookieConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/selectOne")
public class SelectOneServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        HttpSession session = req.getSession();
        int uid = (Integer)(session.getAttribute("uid"));

      String username = req.getParameter("username");
      String password = req.getParameter("password");
        UserDao dao = new UserDaoImp();
        dao.updataUser(new User(uid,username,password));
        PrintWriter pw = resp.getWriter();
        pw.write(" 用户名:<input type='text' name='username' id='username' /> <br />" );
        pw.write("密码:<input type='password' name = 'password' id='password' /></br>");
        pw.write("<input type='submit' value='修改' />");
    }
}
