package top.lrish.www.servlet;

import top.lrish.www.Dao.Imp.UserDaoImp;
import top.lrish.www.Server.Imp.UserServiceImp;
import top.lrish.www.Server.UserService;
import top.lrish.www.entry.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/find")
public class findAllServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter pw = resp.getWriter();
        User ul = (User)req.getSession().getAttribute("loginUser");

        if (ul == null){
            resp.sendRedirect(req.getContextPath()+"login.html");

        }
        UserService service =new UserServiceImp();
        List<User> list = service.findAll();
        System.out.println(list);
        pw.write("<table width='300px' border='1px' cellspacing='0' cellpadding='10px'>");
        pw.write("<tr><th>用户名</th><th>密码</th><th>操作</th></tr>");
        for (User u : list){
            pw.write("<tr><td>"+u.getUsername()+"</td><td>"+u.getPassword()+"</td><td><a href='updata.html?uid="+u.getUid()+"'> 修改</a> | <a href='del?id="+u.getUid()+"'>删除</a></td></tr>");

        }
        pw.write("</table>");
    }
}
