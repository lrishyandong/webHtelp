package top.lrish.www.servlet;

import top.lrish.www.Server.Imp.UserServiceImp;
import top.lrish.www.Server.UserService;
import top.lrish.www.entry.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
@WebServlet("/login")
public class loginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String code = req.getParameter("code");
        String remever = req.getParameter("rememver");
        UserService service = new UserServiceImp();

        User user = new User(username,password);
        boolean flag = service.selectUserByUser(user);
        if (flag){
            HttpSession session = req.getSession();
            String text = (String) (session.getAttribute("text"));
            if (text.equalsIgnoreCase(code)){
                if (remever!=null){
                    Cookie cookie = new Cookie("userinfo",username+":"+password);
                    cookie.setPath("/");
                    cookie.setMaxAge(60*60*7);
                    resp.addCookie(cookie);
                }
                session.setAttribute("loginUser",user);
                resp.sendRedirect(req.getContextPath()+"/find");

            }
        }else {
            req.getRequestDispatcher("/login.html").forward(req,resp);
        }
    }
}
