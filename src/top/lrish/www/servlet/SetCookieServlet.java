package top.lrish.www.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet("/aa/setCookie")
public class SetCookieServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //创建
//        Cookie cookie  = new Cookie("msg","helloword");

             //cookie保存路径,默认cookie保存在访问路径下,会造成其他路径获取不到cookie  建议保存在当前项目路径下
        Cookie cookie  = new Cookie(URLEncoder.encode("姓名","utf-8"),
                URLEncoder.encode("张三","utf-8"));
        cookie.setPath("/");
        cookie.setMaxAge(60*60*24*7);
        resp.addCookie(cookie);
    }
}
